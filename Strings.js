/*
 * Copyright (c) 2013 Soulberto Lorenzo <slorenzot@gmail.com>
 *
 * Licensed under MIT
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 * 
 * brackets-emptyextension
 */
 
/*jslint vars: true, plusplus: true, devel: true, nomen: true, indent: 4, maxerr: 50 */
/*global define, exports, require */


define(function (require, exports, module) {
    'use strict';
    
    var _languages = {
        'en': {
            // commands
            "CMD_CUSTOM"            : 'Custom command',
            "CMD_TOGGLE_CUSTOM"     : 'Enable custom extension',
            // debug
            "DBG_GENERIC_ERROR"     : "[{0}] error: {1}",
            "DBG_BUILD_SUCCESSFUL"  : "[{0}] building successful..."
        },
        'es': {
            // commands
            "CMD_CUSTOM"            : 'Comando personalizado',
            "CMD_TOGGLE_CUSTOM"     : "Activar extensión personalizada",
            // debug
            "DBG_GENERIC_ERROR"     : "[{0}] error: {1}",
            "DBG_BUILD_SUCCESSFUL"  : "[{0}] construcción exitosa..."
        }
    };
    
    function getLanguage(id) {
        return _languages[id];
    }
    
    exports.Strings = getLanguage;
});