/*
 * Copyright (c) 2013 Soulberto Lorenzo <slorenzot@gmail.com>
 *
 * Licensed under MIT
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 * 
 * brackets-emptyextension
 */

/*jslint vars: true, plusplus: true, devel: true, nomen: true, indent: 4, maxerr: 50 */
/*global define, brackets, $, window, exports, require */

define(function (require, exports, module) {
    'use strict';
    var AppInit             = brackets.getModule("utils/AppInit"),
        ExtensionUtils      = brackets.getModule("utils/ExtensionUtils"),
        Dialogs             = brackets.getModule("widgets/Dialogs"),
        DocumentManager     = brackets.getModule("document/DocumentManager"),
        CommandManager      = brackets.getModule("command/CommandManager"),
        PreferencesManager  = brackets.getModule("preferences/PreferencesManager"),
        ProjectManager      = brackets.getModule("project/ProjectManager"),
        StringUtils         = brackets.getModule("utils/StringUtils"),
        Menus               = brackets.getModule("command/Menus");
        
    var appMenu         = Menus.getMenu(Menus.AppMenuBar.EDIT_MENU),
        projectMenu     = Menus.getContextMenu(Menus.ContextMenuIds.PROJECT_MENU);
        
    var Commands    = require('Commands'),
        Languages   = require('Strings'),
        Shortcuts   = require('Shortcuts');
    
    var langs       = Languages.Strings(brackets.app.language); // get app correct language
    
    var settings    = PreferencesManager.getPreferenceStorage(Commands.EXTENSION_ID);
    
    // get bracket jscompress full path
    function getExtensionPath() {
        var selectedItem = ProjectManager.getSelectedItem(),
            file_cwd = selectedItem.fullPath.split('/');
            
        file_cwd.pop();
        
        return file_cwd.join('/');
    }
    
    console.log(DocumentManager);
    
    /**
     * Register commands
     */
    var toggle_extension_cmd, custom_cmd;
    
    // Regiter toggle custom command
    toggle_extension_cmd = CommandManager.register(
        langs.CMD_TOGGLE_CUSTOM,
        Commands.TOGGLE_CUSTOM_CMD,
        function () {
            var active_cmd = !settings.getValue(Commands.TOGGLE_CUSTOM_CMD),
                command = CommandManager.get(Commands.TOGGLE_CUSTOM_CMD);
            
            // change enabled <-> disabled command
            settings.setValue(Commands.TOGGLE_CUSTOM_CMD, active_cmd);
            PreferencesManager.savePreferences();
            
            command.setChecked(active_cmd);
            
            console.log(StringUtils.format("[{0}] Custom extension is active?: {1}", Commands.EXTENSION_ID, active_cmd));
        }
    );
    
    // Register custom command
    custom_cmd = CommandManager.register(langs.CMD_CUSTOM, Commands.CUSTOM_CMD,
        function () {
            var active_cmd = settings.getValue(Commands.TOGGLE_CUSTOM_CMD);
            
            if (!active_cmd) {
                console.log(StringUtils.format('[{0}] Extension command is disabled!', Commands.EXTENSION_ID));
            }
            
            console.log(StringUtils.format('[{0}] Execute extension command', Commands.EXTENSION_ID));
        });
    
    if (appMenu) {
        appMenu.addMenuDivider();
        appMenu.addMenuItem(
            Commands.TOGGLE_CUSTOM_CMD,
            Shortcuts.allPlatforms.TOGGLE_CUSTOM_CMD
        );
    }
                
    if (projectMenu) {
        projectMenu.addMenuDivider();
        projectMenu.addMenuItem(
            Commands.CUSTOM_CMD,
            Shortcuts.allPlatforms.CUSTOM_CMD
        );
    }
        
    // active settings saved previous brackets runnings
    toggle_extension_cmd.setChecked(settings.getValue(Commands.TOGGLE_CUSTOM_CMD)); // enable autocompress
        
    // extension main function
    AppInit.appReady(function () {
        console.log(StringUtils.format('[{0}] Custom extension for brackets', Commands.EXTENSION_ID));
    });
});